const letterCounts = {};
const wordCounts={};
document.getElementById("countButton").onclick = function() {
    // your code will go here ...
 
 let typedText = document.getElementById("textInput").value;
 typedText = typedText.toLowerCase();
// This changes all the letter to lower case.

typedText = typedText.replace(/[^a-z'\s]+/g, "");
// This gets rid of all the characters except letters, spaces, and apostrophes.
// We will learn more about how to use the replace function in an upcoming lesson.
for (let i = 0; i < typedText.length; i++) {
    currentLetter = typedText[i];
    if(letterCounts[currentLetter] === undefined) {
      letterCounts[currentLetter] = 1;
   } else {
      letterCounts[currentLetter]++;
   }
    // do something for each letter.
 }
 

 
 for (let letter in letterCounts) {
    const span = document.createElement("span");
    const textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", ");
    span.appendChild(textContent);
    document.getElementById("lettersDiv").appendChild(span);
 }
 const words = typedText.split(" ");
 
 for (let word of words) {
   currentWord = word;
   if(wordCounts[currentWord] === undefined) {
      wordCounts[currentWord] = 1;
   } 
   else {
      wordCounts[currentWord]++;
   // do something for each letter.
}

}
for (let word in wordCounts) {
   const span = document.createElement("span");
   const textContent = document.createTextNode('"' + word + "\": " + wordCounts[word] + ", ");
   span.appendChild(textContent);
   document.getElementById("wordsDiv").appendChild(span);
}

}